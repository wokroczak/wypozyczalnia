-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Czas generowania: 22 Sty 2019, 10:05
-- Wersja serwera: 5.7.23
-- Wersja PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `wypozyczalnia`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `samochody`
--

DROP TABLE IF EXISTS `samochody`;
CREATE TABLE IF NOT EXISTS `samochody` (
  `Rejestracja` varchar(10) NOT NULL,
  `Marka` varchar(20) NOT NULL,
  `Model` varchar(20) NOT NULL,
  `Kolor` varchar(20) NOT NULL,
  `Obraz` varchar(50) NOT NULL,
  `Paliwo` varchar(20) NOT NULL,
  `Moc` int(5) NOT NULL,
  `Cena` int(20) NOT NULL,
  PRIMARY KEY (`Rejestracja`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `samochody`
--

INSERT INTO `samochody` (`Rejestracja`, `Marka`, `Model`, `Kolor`, `Obraz`, `Paliwo`, `Moc`, `Cena`) VALUES
('RK39000', 'Opel', 'Crossland X', 'Biały', 'https://i.imgur.com/92qFWUh.png', 'Benzyna', 120, 100),
('RK39001', 'Opel', 'Grandland X', 'Niebieski', 'https://i.imgur.com/30LvX2b.png', 'Benzyna', 120, 110),
('RK39002', 'Opel', 'Movano Furgon', 'Czerwony', 'https://i.imgur.com/y2uVopr.png', 'Diesel', 100, 150),
('RK39003', 'Opel', 'Adam Rocks', 'Biały', 'https://i.imgur.com/sU4rw9G.png', 'Benzyna', 90, 80),
('RK39004', 'Opel', 'Zafira', 'Biały', 'https://i.imgur.com/aPoD7kK.png', 'Benzyna', 140, 120),
('RK39005', 'Opel', 'GTC', 'Czerwony', 'https://i.imgur.com/qbFA0Yd.png', 'Benzyna', 220, 180),
('RK39006', 'Opel', 'Cascada', 'Szary', 'https://i.imgur.com/xYmHVlt.png', 'Benzyna', 180, 180),
('RK39007', 'Opel', 'Astra Sedan', 'Brązowy', 'https://i.imgur.com/AZeiUAX.png', 'Benzyna', 100, 110),
('RK39008', 'Opel', 'Insignia Country', 'Srebrny', 'https://i.imgur.com/X2POB99.png', 'Diesel', 210, 200),
('RK39009', 'Opel', 'Astra Tourer', 'Srebrny', 'https://i.imgur.com/5Z7AhHd.png', 'Benzyna', 120, 130),
('RK39010', 'Opel', 'Vivaro Furgon', 'Czerwony', 'https://i.imgur.com/sR1kDeJ.png', 'Diesel', 120, 150),
('RK39011', 'Opel', 'Vivaro Kombi', 'Czerwony', 'https://i.imgur.com/KjYl02K.png', 'Diesel', 160, 160),
('RK39012', 'Opel', 'Insignia Tourer', 'Srebrny', 'https://i.imgur.com/Oiv1KjG.png', 'Diesel', 180, 170),
('RK39013', 'Opel', 'Insignia Sport', 'Niebieski', 'https://i.imgur.com/T8wUsHj.png', 'Benzyna', 200, 180),
('RK39014', 'Opel', 'Insigna GSI', 'Szary', 'https://i.imgur.com/NP7PqZz.png', 'Benzyna', 280, 220),
('RK39015', 'Opel', 'Mokka X', 'Pomarańczowy', 'https://i.imgur.com/CeUBBQx.png', 'Benzyna', 120, 140),
('RK39016', 'Opel', 'Adam S', 'Czarny', 'https://i.imgur.com/WDVJXfW.png', 'Benzyna', 160, 160),
('RK39017', 'Opel', 'Corsa 3D', 'Czerwony', 'https://i.imgur.com/aXnbVeA.png', 'Benzyna', 90, 100),
('RK39018', 'Opel', 'Adam', 'Niebieski', 'https://i.imgur.com/FQuydhS.png', 'Benzyna', 70, 90),
('RK39019', 'Opel', 'Corsa 5D', 'Szary', 'https://i.imgur.com/QslGNTw.png', 'Benzyna', 90, 100),
('RK39020', 'Opel', 'Movano Kombi', 'Srebrny', 'https://i.imgur.com/gRoxUi5.png', 'Diesel', 150, 180),
('RK39021', 'Opel', 'Combo', 'Niebieski', 'https://i.imgur.com/hZ2bhm8.png', 'Diesel', 100, 160),
('RK39022', 'Opel', 'Astra', 'Czerwony', 'https://i.imgur.com/FYbQW3f.png', 'Diesel', 120, 170);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `transakcje`
--

DROP TABLE IF EXISTS `transakcje`;
CREATE TABLE IF NOT EXISTS `transakcje` (
  `TransakcjaID` int(11) NOT NULL AUTO_INCREMENT,
  `LoginID` varchar(20) NOT NULL,
  `RejestracjaID` varchar(10) NOT NULL,
  `DataOD` date NOT NULL,
  `DataDO` date NOT NULL,
  `Cena` int(50) NOT NULL,
  PRIMARY KEY (`TransakcjaID`),
  KEY `Transakcje_fk0` (`LoginID`),
  KEY `Transakcje_fk1` (`RejestracjaID`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `transakcje`
--

INSERT INTO `transakcje` (`TransakcjaID`, `LoginID`, `RejestracjaID`, `DataOD`, `DataDO`, `Cena`) VALUES
(1, 'adam', 'RK39000', '2019-01-14', '2019-01-23', 500),
(2, 'adam', 'RK39001', '2019-01-10', '2019-01-15', 500),
(3, 'adam', 'RK39004', '2019-01-28', '2019-01-30', 400),
(4, 'adam', 'RK39003', '2019-01-10', '2019-01-20', 880),
(5, 'adam', 'RK39004', '2019-01-11', '2019-01-21', 1320),
(6, 'adam', 'RK39006', '2019-01-15', '2019-01-25', 1980),
(7, 'adam', 'RK39007', '2019-01-15', '2019-01-25', 1210),
(8, 'adam', 'RK39000', '2019-02-08', '2019-02-21', 1400),
(9, 'adam', 'RK39001', '2019-02-07', '2019-02-21', 1650),
(10, 'adam', 'RK39002', '2019-02-07', '2019-02-21', 2250),
(11, 'adam', 'RK39003', '2019-02-07', '2019-02-21', 1200),
(12, 'adam', 'RK39004', '2019-02-07', '2019-02-21', 1800),
(13, 'adam', 'RK39005', '2019-02-07', '2019-02-21', 2700),
(14, 'adam', 'RK39006', '2019-02-07', '2019-02-21', 2700);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uzytkownicy`
--

DROP TABLE IF EXISTS `uzytkownicy`;
CREATE TABLE IF NOT EXISTS `uzytkownicy` (
  `Login` varchar(20) NOT NULL,
  `Haslo` varchar(20) NOT NULL,
  `Email` varchar(75) NOT NULL,
  `Imie` varchar(20) NOT NULL,
  `Nazwisko` varchar(20) NOT NULL,
  `Telefon` text NOT NULL,
  `Typ` tinyint(1) NOT NULL,
  PRIMARY KEY (`Login`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `uzytkownicy`
--

INSERT INTO `uzytkownicy` (`Login`, `Haslo`, `Email`, `Imie`, `Nazwisko`, `Telefon`, `Typ`) VALUES
('adam', 'haslo', 'adam@o2.pl', 'Adam', 'Nowak', '123456789', 0),
('admin', 'admin', 'admin@wypozycz.pl', 'admin', 'admin', '000000000', 1),
('adam2', 'adamhaslo', 'adam@tlen.de', 'adam', 'nazwisko', '1121213', 0),
('adam3', 'adamhaslo', 'adam@tlen.de', 'adam', 'nazwisko', '1121213', 0),
('adam5', 'adamhaslo', 'adam@tlen.de', 'adam', 'nazwisko', '1121213', 0),
('adame', 'adame', 'adame@o2.pl', 'adame', 'adame', '1121313', 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
