import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailATComponent } from './detail-at.component';

describe('DetailATComponent', () => {
  let component: DetailATComponent;
  let fixture: ComponentFixture<DetailATComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailATComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailATComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
