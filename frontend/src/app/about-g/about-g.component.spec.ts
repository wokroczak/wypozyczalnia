import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutGComponent } from './about-g.component';

describe('AboutGComponent', () => {
  let component: AboutGComponent;
  let fixture: ComponentFixture<AboutGComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutGComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutGComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
