import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  users: Object;
  foods: Object;
  cars: Object;

  constructor(private data: DataService) { }

  ngOnInit() {


    this.data.getUsers().subscribe(data => {
      this.users = data
      console.log(this.users);
    }
    );




     this.data.getCars().subscribe(
      data => { this.cars = data},
      err => console.error(err),
      () => console.log(this.cars)
    );








  }

}
