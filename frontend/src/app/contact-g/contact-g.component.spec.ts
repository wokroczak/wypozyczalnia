import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactGComponent } from './contact-g.component';

describe('ContactGComponent', () => {
  let component: ContactGComponent;
  let fixture: ComponentFixture<ContactGComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactGComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactGComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
