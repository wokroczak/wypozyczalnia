import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Validator } from '@angular/forms';

@Component({
  selector: 'app-detail-a',
  templateUrl: './detail-a.component.html',
  styleUrls: ['./detail-a.component.css']
})
export class DetailAComponent implements OnInit {

  obraz1:string;
  obraz2:string;
  cars: Object;
  updatecar: Object;

  rejestracja: string;
  marka: string;
  model: string;
  kolor: string;
  obraz: string;
  paliwo: string;
  moc: number;
  cena: number;



  constructor(private data: DataService, private activatedRoute: ActivatedRoute, public router: Router)  { }

  ngOnInit() {

    

    this.activatedRoute.params.subscribe(params => {
      console.log(params) 
      console.log(params['id']) 


      this.data.getCar(params['id']).subscribe(data => {
        this.cars = data
        console.log(this.cars);
        this.obraz1 = data[0].Obraz;
        this.obraz2 = this.obraz1.substring(20)
        console.log(this.obraz2);
      }
      );


    });



  }





  update(autorejestracja:string, automarka:string, automodel:string, autokolor:string, autoobraz:string, autopaliwo:string, automoc:number, autocena:number) {

    console.log(autorejestracja,automarka, automodel, autokolor, autoobraz, autopaliwo, automoc, autocena);


   
                        this.data.updateCar(autorejestracja,automarka,automodel,autokolor,autoobraz,autopaliwo,automoc,autocena).subscribe(data => {
                          this.updatecar = data
                          console.log(this.updatecar);
                          if (this.updatecar) {
                            console.log("zaktualizowano");
                            this.router.navigateByUrl('/admincar');
    
                          } else {
                            console.log("error");
                          }
                        });
                     
  }




}
