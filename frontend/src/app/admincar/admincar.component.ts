import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admincar',
  templateUrl: './admincar.component.html',
  styleUrls: ['./admincar.component.css']
})
export class AdmincarComponent implements OnInit {


  users: Object;
  foods: Object;
  cars: Object;

  addcar: Object;


  constructor(private data: DataService, public router: Router) { }

  ngOnInit() {
   
    this.data.getUsers().subscribe(data => {
      this.users = data
      console.log(this.users);
    }
    );


    

    this.data.getCars().subscribe(
      data => { this.cars = data },
      err => console.error(err),
      () => console.log(this.cars)
    );





  }


  rejestracja: string;
  marka: string;
  model: string;
  kolor: string;
  obraz: string;
  paliwo: string;
  moc: number;
  cena: number;




  usun(rejestracja) { 
    console.log(rejestracja);
    this.data.deleteCar(rejestracja).subscribe(data => {
      this.cars = data
      console.log(this.cars);
      if (this.cars=1) {
        console.log("usuniety");
        setTimeout(
          function(){ 
          location.reload(); 
          }, 500);
      } else {
        console.log("nie usunietto");
      }
  });
}




  submit() {

    console.log(this.rejestracja, this.marka, this.model, this.kolor, this.obraz, this.paliwo, this.moc, this.cena);


    this.data.rejestracjaCheck(this.rejestracja).subscribe(data => {
      this.cars = data

      console.log(this.cars);
      if (this.cars) {
        console.log("jest juz takie auto");

      } else {
        console.log("nie ma takiego auta");
     
        if (this.rejestracja) {
          if (this.marka) {
            if (this.model) {
              if (this.kolor) {
                if (this.obraz) {
                  if (this.paliwo) {
                    if (this.moc) {
                      if (this.cena) {
                        console.log("MOZNA DODAWAC AUTO");
                        
                        this.data.addcar(this.rejestracja, this.marka, this.model, this.kolor, this.obraz, this.paliwo, this.moc, this.cena).subscribe(data => {
                          this.addcar = data
                          console.log(this.addcar);
                          if (this.addcar) {
                            console.log("dodano");
                            setTimeout(
                              function(){ 
                              location.reload(); 
                              }, 500);
    
                          } else {
                            console.log("error");
                          }
                        });
                      } else {
                        console.log("podaj cene")
                      }
                    } else {
                      console.log("podaj moc")
                    }
                  } else {
                    console.log("podaj paliwo")
                  }
                } else {
                  console.log("podaj obraz")
                }
              } else {
                console.log("podaj kolor")
              }
            } else {
              console.log("podaj model")
            }
          } else {
            console.log("podaj marke")
          }
        } else {
          console.log("podaj rejestracje")
        }


      }
    });
  }


}
