import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailGComponent } from './detail-g.component';

describe('DetailGComponent', () => {
  let component: DetailGComponent;
  let fixture: ComponentFixture<DetailGComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailGComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailGComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
