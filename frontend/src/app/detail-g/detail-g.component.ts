import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail-g',
  templateUrl: './detail-g.component.html',
  styleUrls: ['./detail-g.component.css']
})
export class DetailGComponent implements OnInit {

  cars: Object;

  constructor(private data: DataService, private activatedRoute: ActivatedRoute)  { }

  ngOnInit() {

    this.activatedRoute.params.subscribe(params => {
      console.log(params) 
      console.log(params['id']) 


      this.data.getCar(params['id']).subscribe(data => {
        this.cars = data
        console.log(this.cars);
      }
      );


    });


  }

}
