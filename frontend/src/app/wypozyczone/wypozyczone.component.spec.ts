import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WypozyczoneComponent } from './wypozyczone.component';

describe('WypozyczoneComponent', () => {
  let component: WypozyczoneComponent;
  let fixture: ComponentFixture<WypozyczoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WypozyczoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WypozyczoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
