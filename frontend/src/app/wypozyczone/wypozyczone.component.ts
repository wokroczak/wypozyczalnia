import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-wypozyczone',
  templateUrl: './wypozyczone.component.html',
  styleUrls: ['./wypozyczone.component.css']
})
export class WypozyczoneComponent implements OnInit {

  cars: Object;
 

  constructor(private data: DataService) { }

  ngOnInit() {
    let login = localStorage.getItem('login');
    this.data.getTrans(login).subscribe(data => {
      this.cars = data
      console.log(this.cars);

    });
    
  }

}
