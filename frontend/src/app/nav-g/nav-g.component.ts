import { Component, OnInit } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

@Component({
  selector: 'app-nav-g',
  templateUrl: './nav-g.component.html',
  styleUrls: ['./nav-g.component.css']
})
export class NavGComponent implements OnInit {

  navbarOpen = false;

  constructor() { }

  ngOnInit() {
  }


  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }
  
}
