import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { OfertaComponent } from './oferta/oferta.component';
import { RegulaminComponent } from './regulamin/regulamin.component';

import { HttpClientModule } from '@angular/common/http';
import { DetailComponent } from './detail/detail.component';
import { NavGComponent } from './nav-g/nav-g.component';
import { DetailGComponent } from './detail-g/detail-g.component';
import { AboutGComponent } from './about-g/about-g.component';
import { ContactGComponent } from './contact-g/contact-g.component';
import { AdminComponent } from './admin/admin.component';
import { AdminuserComponent } from './adminuser/adminuser.component';
import { AdmincarComponent } from './admincar/admincar.component';
import { AdmintransComponent } from './admintrans/admintrans.component';
import { NavAComponent } from './nav-a/nav-a.component';
import { DetailAComponent } from './detail-a/detail-a.component';
import { DetailAUComponent } from './detail-au/detail-au.component';
import { DetailATComponent } from './detail-at/detail-at.component';
import { DataComponent } from './data/data.component';
import { WypozyczoneComponent } from './wypozyczone/wypozyczone.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    AboutComponent,
    ContactComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    OfertaComponent,
    RegulaminComponent,
    DetailComponent,
    NavGComponent,
    DetailGComponent,
    AboutGComponent,
    ContactGComponent,
    AdminComponent,
    AdminuserComponent,
    AdmincarComponent,
    AdmintransComponent,
    NavAComponent,
    DetailAComponent,
    DetailAUComponent,
    DetailATComponent,
    DataComponent,
    WypozyczoneComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
