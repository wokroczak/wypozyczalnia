import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailAUComponent } from './detail-au.component';

describe('DetailAUComponent', () => {
  let component: DetailAUComponent;
  let fixture: ComponentFixture<DetailAUComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailAUComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailAUComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
