import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Validator } from '@angular/forms';
import { AfterViewInit,ElementRef } from '@angular/core';

@Component({
  selector: 'app-detail-au',
  templateUrl: './detail-au.component.html',
  styleUrls: ['./detail-au.component.css']
})
export class DetailAUComponent implements OnInit {


  users: Object;
  updateuser: Object;

  login: string;
  haslo: string;
  email: string;
  imie: string;
  nazwisko: string;
  telefon: number;
  typ: string;
  clicked:number=0;



  constructor(private data: DataService, private activatedRoute: ActivatedRoute, public router: Router) { }

  ngOnInit() {

    this.activatedRoute.params.subscribe(params => {
      console.log(params) 
      console.log(params['id']) 


      this.data.getUser(params['id']).subscribe(data => {
        this.users = data
        console.log(this.users);
      }
      );


    });










  }

settyp(typ:number){
  this.clicked=1;
  if(typ==0){
    this.typ='0';
  }else{
    this.typ='1';
  }
}


  update(userlogin:string, userhaslo:string, useremail:string, userimie:string, usernazwisko:string, usertelefon:number, usertyp:string) {

   if(this.clicked==0){
    this.data.updateUser(userlogin, userhaslo, useremail, userimie, usernazwisko, usertelefon, usertyp).subscribe(data => {
      this.updateuser = data
      console.log(this.updateuser);
      if (this.updateuser) {
        console.log("zaktualizowano");
        this.router.navigateByUrl('/adminuser');

      } else {
        console.log("error");
      }
    });
   }else{
    this.data.updateUser(userlogin, userhaslo, useremail, userimie, usernazwisko, usertelefon, this.typ).subscribe(data => {
      this.updateuser = data
      console.log(this.updateuser);
      if (this.updateuser) {
        console.log("zaktualizowano");
        this.router.navigateByUrl('/adminuser');

      } else {
        console.log("error");
      }
    });
   }
                        
                     
  }











}
