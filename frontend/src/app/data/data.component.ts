import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import {NgbDate, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
import { stringify } from 'querystring';
import { Router } from '@angular/router';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css']
})
export class DataComponent implements OnInit {

 
  users: Object;
  foods: Object;
  cars: Object;

  hoveredDate: NgbDate;

  fromDate: NgbDate;
  toDate: NgbDate;

  constructor(calendar: NgbCalendar,private data: DataService, public router: Router) {
    this.fromDate = calendar.getToday();
    this.toDate = calendar.getNext(calendar.getToday(), 'd', 10);
  }

  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
      this.toDate = date;
      console.log("data od ",this.fromDate);
      console.log("data do ",this.toDate);
    } else {
      this.toDate = null;
      this.fromDate = date; 
    }
  }

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHovered(date);
  }

  submit(){

   
var DataOd;
var DataDo;


DataOd = JSON.stringify(this.fromDate)
DataDo = JSON.stringify(this.toDate)
console.log(DataOd, DataDo)
localStorage.setItem('dataod', DataOd);
localStorage.setItem('datado', DataDo);


var dataod1 = JSON.parse(DataOd);
var datado1 = JSON.parse(DataDo);

var dataod2 = dataod1.year+'-'+dataod1.month+'-'+dataod1.day
var dataod3 = new Date(dataod1.year, dataod1.month, dataod1.day);
console.log(dataod3)


var datado2 = datado1.year+'-'+datado1.month+'-'+datado1.day;
var datado3 = new Date(datado1.year, datado1.month, datado1.day);
console.log(datado3)

var diff = Math.abs(dataod3.getTime() - datado3.getTime());
var dni = Math.ceil(diff / (1000 * 3600 * 24))+1; 
console.log(dni)
localStorage.setItem('dni', dni.toString());

this.router.navigateByUrl('/oferta');





  }

  ngOnInit() {
  }

}
