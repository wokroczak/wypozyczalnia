import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';


@Component({
  selector: 'app-admintrans',
  templateUrl: './admintrans.component.html',
  styleUrls: ['./admintrans.component.css']
})
export class AdmintransComponent implements OnInit {

  users: Object;
  foods: Object;
  cars: Object;
  transs: Object;

  constructor(private data: DataService) { }

  ngOnInit() {


    this.data.getTranss().subscribe(
      data => { this.transs = data},
      err => console.error(err),
      () => console.log(this.transs)
    );

  }

}
