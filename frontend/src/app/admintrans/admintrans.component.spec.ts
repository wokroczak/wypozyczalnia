import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmintransComponent } from './admintrans.component';

describe('AdmintransComponent', () => {
  let component: AdmintransComponent;
  let fixture: ComponentFixture<AdmintransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdmintransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmintransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
