import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  cars: Object;
  dataod:string = localStorage.getItem('dataod');
  datado:string = localStorage.getItem('datado');
  userlogin:string = localStorage.getItem('login');
  obj:object;
  obj2:object;
  cenacalosc:number;
  rok1:string;
  rok2:string;
  month1:string;
  month2:string;
  day1:string;
  day2:string;

  constructor(private data: DataService, private activatedRoute: ActivatedRoute, public router: Router)  { }

  ngOnInit() {
    let dataod = localStorage.getItem('dataod');
    let datado = localStorage.getItem('datado');
    let dni = localStorage.getItem('dni');
    var dniint = parseInt(dni)
    let obj = JSON.parse(dataod)
    let obj2 = JSON.parse(datado)
    this.rok1=obj.year;
    this.rok2=obj2.year;
    this.month1=obj.month;
    this.month2=obj2.month;
    this.day1=obj.day;
    this.day2=obj2.day;
    console.log(obj)
    console.log(this.rok1,this.month1,this.day1)
    console.log(obj2)
    console.log(this.rok2,this.month2,this.day2)



    this.activatedRoute.params.subscribe(params => {
      console.log(params) 
      console.log(params['id']) 

      this.data.getCar(params['id']).subscribe(data => {
        this.cars = data
        console.log(this.cars);
        var cenacala=data[0].Cena;

        console.log(data[0].cena)
      data[0].Cena=data[0].Cena*dniint;
      console.log(data[0].cena)

        localStorage.setItem('cenacala', cenacala.toString());
        this.cenacalosc =data[0].Cena;


      }
      );


    });




let cenacala2 = localStorage.getItem('cenacala');


  }

  submit(){
    console.log(this.cars[0].Rejestracja);
    let login = localStorage.getItem('login');
    console.log(login);
    console.log(this.dataod);
    console.log(this.datado);
    console.log(this.cenacalosc);

    this.data.transakcja(login, this.cars[0].Rejestracja, this.dataod, this.datado, this.cenacalosc).subscribe(data => {
      this.cars = data
      console.log(this.cars);
    });
    this.router.navigateByUrl('/wypozyczone');
  }

}
