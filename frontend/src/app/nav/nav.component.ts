import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  
  
  userlogin:string = localStorage.getItem('login');
  navbarOpen = false;
  
  constructor() { }

  ngOnInit() {
    
    let login = localStorage.getItem('login');
    let typ = localStorage.getItem('typ');

  }

  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }

  wyloguj(){
    localStorage.clear();
  }

}

