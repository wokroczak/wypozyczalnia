import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-adminuser',
  templateUrl: './adminuser.component.html',
  styleUrls: ['./adminuser.component.css']
})
export class AdminuserComponent implements OnInit {


  users: Object;

  model = 1;
  adduser: Object;


  selectedLink: string = "0";

  setradio(e: string): void {
    this.selectedLink = e;
  }
  isSelected(name: string): boolean {
    if (!this.selectedLink) {  
      return false;
    }
    return (this.selectedLink === name); 
  }






  constructor(private data: DataService) { }

  public radioData: any;

  ngOnInit() {

 this.radioData = 0;


     this.data.getUsers().subscribe(
      data => { this.users = data},
      err => console.error(err),
      () => console.log(this.users)
    );


  }


  login: string;
  haslo: string;
  email: string;
  imie: string;
  nazwisko: string;
  telefon: number;
  typ: boolean;
  myRadio: string=' ';


  usun(login) { 
    console.log(login);
    this.data.deleteUser(login).subscribe(data => {
      this.users = data
      console.log(this.users);
      if (this.users=1) {
        console.log("usuniety");
        setTimeout(
          function(){ 
          location.reload(); 
          }, 500);
      } else {
        console.log("nie usunietto");
      }
  });
}

  submit() {

    console.log(this.login, this.haslo, this.email, this.imie, this.nazwisko, this.telefon, this.selectedLink);



    if (this.validateEmail(this.email)) {
      console.log("poprawny")

    this.data.userCheck(this.login).subscribe(data => {
      this.users = data

      console.log(this.users);
      if (this.users) {
        console.log("jest juz taki user");

      } else {
        console.log("nie ma takiego usera");
    
        if (this.login) {
          if (this.haslo) {
            if (this.email) {
              if (this.imie) {
                if (this.nazwisko) {
                  if (this.telefon) {
                    if (this.selectedLink) {
                        console.log("MOZNA DODAWAC USERA");

                        this.data.adduser(this.login, this.haslo, this.email, this.imie, this.nazwisko, this.telefon, this.selectedLink).subscribe(data => {
                          this.adduser = data
                          console.log(this.adduser);
                          if (this.adduser) {
                            console.log("dodano");
                            setTimeout(
                              function(){ 
                              location.reload(); 
                              }, 500);
    
                          } else {
                            console.log("error");
                          }
                        });
                        
                    } else {
                      console.log("podaj typ")
                    }
                  } else {
                    console.log("podaj telefon")
                  }
                } else {
                  console.log("podaj nazwisko")
                }
              } else {
                console.log("podaj imie")
              }
            } else {
              console.log("podaj email")
            }
          } else {
            console.log("podaj haslo")
          }
        } else {
          console.log("podaj login")
        }


      }
    });
  } else {
    console.log("niepoprawny")

  
  }

}



validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}


}
