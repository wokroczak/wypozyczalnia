import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  users: Object;
  register: Object;

  constructor(private data: DataService, public router: Router) { }

  ngOnInit() {
  }

  login: string;
  email: string;
  haslo: string;
  imie: string;
  nazwisko: string;
  telefon: number;


  submit() {

    console.log(this.login, this.email, this.haslo, this.imie, this.nazwisko, this.telefon);





    if (this.validateEmail(this.email)) {
      console.log("poprawny")
      this.data.loginCheck(this.login).subscribe(data => {
      this.users = data

        console.log(this.users);
        if (this.users) {
          console.log("jest juz taki user");

        } else {
          console.log("nie ma takiego usera");
         

          if (this.login) {
            if (this.haslo) {
              if (this.imie) {
                if (this.nazwisko) {
                  if (this.telefon) {
                    console.log("MOZNA REJESTROWAC");
                    this.data.register(this.login, this.email, this.haslo, this.imie, this.nazwisko, this.telefon).subscribe(data => {
                      this.register = data
                      console.log(this.register);
                      if (this.register) {
                        console.log("dodano");
                        var login = this.login;
                        var typ = "0";
                        console.log(login);
                        console.log(typ);

                        localStorage.setItem('login', login);
                        localStorage.setItem('typ', typ);

                          this.router.navigateByUrl('/oferta');

                      } else {
                        console.log("error");
                      }
                    });
                  } else {
                    console.log("podaj telefon")
                  }
                } else {
                  console.log("podaj nazwisko")
                  
                }
              } else {
                console.log("podaj imie")
              }
            } else {
              console.log("podaj haslo")
            }
          } else {
            console.log("podaj login")
          }



        }
      });

    } else {
      console.log("niepoprawny")

     
    }
  }

  validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

}
