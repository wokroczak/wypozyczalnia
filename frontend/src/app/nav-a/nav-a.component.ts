import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav-a',
  templateUrl: './nav-a.component.html',
  styleUrls: ['./nav-a.component.css']
})
export class NavAComponent implements OnInit {

  userlogin:string = localStorage.getItem('login');
  navbarOpen = false;


  constructor() { }

  ngOnInit() {
    let login = localStorage.getItem('login');
    let typ = localStorage.getItem('typ');

  }

  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }

  wyloguj(){
    localStorage.clear();
  }

}
