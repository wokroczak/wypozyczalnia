import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { OfertaComponent } from './oferta/oferta.component';
import { DetailComponent } from './detail/detail.component';
import { DetailAComponent } from './detail-a/detail-a.component';
import { DetailAUComponent } from './detail-au/detail-au.component';
import { DetailATComponent } from './detail-at/detail-at.component';
import { AboutGComponent } from './about-g/about-g.component';
import { ContactGComponent } from './contact-g/contact-g.component';
import { DetailGComponent } from './detail-g/detail-g.component';
import { AdminComponent } from './admin/admin.component';
import { AdminuserComponent } from './adminuser/adminuser.component';
import { AdmincarComponent } from './admincar/admincar.component';
import { AdmintransComponent } from './admintrans/admintrans.component';
import { DataComponent } from './data/data.component';
import { WypozyczoneComponent } from './wypozyczone/wypozyczone.component';

const routes: Routes = [
  { path: 'about', component: AboutComponent },
  { path: 'contact', component: ContactComponent },
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'oferta', component: OfertaComponent },
  { path: 'detail/:id', component: DetailComponent },
  { path: 'detail-a/:id', component: DetailAComponent },
  { path: 'detail-at/:id', component: DetailATComponent },
  { path: 'detail-au/:id', component: DetailAUComponent },
  { path: 'aboutg', component: AboutGComponent },
  { path: 'contactg', component: ContactGComponent },
  { path: 'detailg/:id', component: DetailGComponent },
  { path: 'admin', component: AdminComponent },
  { path: 'adminuser', component: AdminuserComponent },
  { path: 'admincar', component: AdmincarComponent },
  { path: 'admintrans', component: AdmintransComponent },
  { path: 'data', component: DataComponent },
  { path: 'wypozyczone', component: WypozyczoneComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
