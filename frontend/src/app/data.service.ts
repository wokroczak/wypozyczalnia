import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})

export class DataService {

  constructor(private http: HttpClient) { }

  getUsers() {
 

    return this.http.get('http://127.0.0.1:8081/listUsers')
  }



  getCars() {
    return this.http.get('http://127.0.0.1:8081/listCars')
  }

  getTranss() {
    return this.http.get('http://127.0.0.1:8081/listTrans')
  }


  getCar(id:number) {
    return this.http.get(`http://127.0.0.1:8081/listCar/${id}`)
  }

  getUser(login:string) {
    return this.http.get(`http://127.0.0.1:8081/listUser/${login}`)
  }

  getLogin(login:string, password:string) {
    return this.http.get(`http://127.0.0.1:8081/login/${login}/${password}`)
  }

  getDostepne(dataod:string, datado:string) {
    return this.http.get(`http://127.0.0.1:8081/dostepne/${dataod}/${datado}`)
  }



  getTrans(login:string) {
    return this.http.get(`http://127.0.0.1:8081/gettrans/${login}/`)
  }

  loginCheck(login:string) {
    return this.http.get(`http://127.0.0.1:8081/logincheck/${login}`)
  }

  rejestracjaCheck(rejestracja:string) {
    return this.http.get(`http://127.0.0.1:8081/rejestracjacheck/${rejestracja}`)
  }

  userCheck(login:string) {
    return this.http.get(`http://127.0.0.1:8081/logincheck/${login}`)
  }

  deleteCar(rejestracja:string) {
    return this.http.get(`http://127.0.0.1:8081/deletecar/${rejestracja}`)
  }


  deleteUser(login:string) {
    return this.http.get(`http://127.0.0.1:8081/deleteuser/${login}`)
  }

  register(login:string, email:string, haslo:string, imie:string, nazwisko:string, telefon:number) {
    return this.http.get(`http://127.0.0.1:8081/register/${login}/${email}/${haslo}/${imie}/${nazwisko}/${telefon}`)
  }

  transakcja(login:string, rejestracja:string, dataod:string, datado:string, cena:number) {
    return this.http.get(`http://127.0.0.1:8081/transakcja/${login}/${rejestracja}/${dataod}/${datado}/${cena}/`)
  }

  addcar(rejestracja:string, marka:string, model:string, kolor:string, obraz:string, paliwo:string, moc:number, cena:number) {
    return this.http.get(`http://127.0.0.1:8081/addcar/${rejestracja}/${marka}/${model}/${kolor}/${obraz}/${paliwo}/${moc}/${cena}`)
  }


  updateCar(rejestracja:string, marka:string, model:string, kolor:string, obraz:string, paliwo:string, moc:number, cena:number) {
    return this.http.get(`http://127.0.0.1:8081/updatecar/${rejestracja}/${marka}/${model}/${kolor}/${obraz}/${paliwo}/${moc}/${cena}`)
  }

  updateUser(login:string, haslo:string, email:string, imie:string, nazwisko:string, telefon:number, typ:string) {
    return this.http.get(`http://127.0.0.1:8081/updateuser/${login}/${haslo}/${email}/${imie}/${nazwisko}/${telefon}/${typ}`)
  }


  adduser(login:string, haslo:string, email:string, imie:string, nazwisko:string, telefon:number, typ:string) {
    return this.http.get(`http://127.0.0.1:8081/adduser/${login}/${haslo}/${email}/${imie}/${nazwisko}/${telefon}/${typ}`)
  }


  firstClick() {
    return console.log('clicked');
  }


}
